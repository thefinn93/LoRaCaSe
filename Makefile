lora-case.gcode: lora-case.stl
	prusaslicer -s -g --info -o lora-case.gcode lora-case.stl

lora-case.stl: lora-case.scad
	openscad -o lora-case.stl -D "version_string=\"$(shell git describe --always)\"" lora-case.scad

lora-case.3mf: lora-case.scad
	openscad -o lora-case.3mf -D "version_string=\"$(shell git describe --always)\"" lora-case.scad
