// Misc
padding_size = 2;
height_padding = 5;

// Solar Panel
panel_length = 110.2;
panel_width = 60.2;
panel_height = 3;
panel_angle = 45; // https://www.solarpaneltilt.com says 37.3 for the 45N (PNW)
panel_frame_height = 2;
panel_notch_size = 10;

// Lid
lid_notch_size = 5;
lid_internal_height = 3;
lid_height = lid_notch_size + lid_internal_height + panel_height;
lid_hole_diameter = 20;

// Battery Holder
battery_holder_length = 77.3;
battery_holder_width = 22.3;
battery_holder_height = 21.5;

wire_guide_size = 2;

// TTGO module
ttgo_module_length = 60;
ttgo_module_width = 25;
ttgo_module_height = 16;
ttgo_module_antenna_connector_length = 8;
ttgo_module_antenna_connector_diameter = 9.5;
//ttgo_module_antenna_connector_height = 4; // When measuring from other side of the PCB
ttgo_module_antenna_connector_height = 10; // when measuring from the top of the screen
ttgo_module_antenna_connector_inset = 12;
ttgo_module_screen_x = 5;
ttgo_module_screen_y = 10;
ttgo_module_screen_length = 20; // measures 33.3 on the calipers but more wiggle room may be needed
ttgo_module_screen_width = 34; // Measure 18.64, but the cable may require more wiggle room.

// Version String
version_depth = 2;
version_size = 10;
version_string = "dev"; // Set externally when building in CI

// computed from above
inner_diff = padding_size*2;

width = panel_width + inner_diff;
length = panel_length + inner_diff;
height = battery_holder_height+lid_height+height_padding;


module lid_base() {
    difference() {
        cube([width, length, lid_height]);
        translate([padding_size, padding_size]) cube([width-padding_size*2, length-padding_size*2, lid_notch_size]);
    }
}

module lid() {
    difference() {
        lid_base();

        translate([0, 0, lid_notch_size])
        versionstamp();

        translate([padding_size, padding_size, lid_notch_size+lid_internal_height])
        cube([panel_width, panel_length, panel_height]);

        translate([width/2, length/2, lid_notch_size])
        linear_extrude(lid_internal_height)
        circle(d=lid_hole_diameter);
    }
}

module battery_holder() {
    translate([padding_size*2, (length/2)-battery_holder_length/2])
    cube([battery_holder_width, battery_holder_length, battery_holder_height]);

}

module wire_guides() {
    translate([padding_size*2-2, (length/2)-battery_holder_length/2])
    union() {
        translate([battery_holder_width-wire_guide_size, wire_guide_size*-1-1])
        linear_extrude(battery_holder_height) square([wire_guide_size, 3]);
        
        translate([battery_holder_width-wire_guide_size, battery_holder_length])
        linear_extrude(battery_holder_height) square([wire_guide_size, 3]);
    }
}

module ttgo_module() {
    module_x = width/2-padding_size;
    module_y = length/2-ttgo_module_length/2-inner_diff;

    translate([module_x, module_y])
        cube([ttgo_module_width, ttgo_module_length, height-inner_diff-panel_height]);

    translate([module_x+ttgo_module_screen_x, module_y+ttgo_module_screen_y, -1])
        cube([ttgo_module_screen_length, ttgo_module_screen_width, inner_diff]);
}

module antenna_connector() {
    hole_z = ttgo_module_antenna_connector_height + ttgo_module_antenna_connector_diameter/2 + inner_diff;
    hole_y = length/2+ttgo_module_length/2-ttgo_module_antenna_connector_inset-ttgo_module_antenna_connector_diameter/2;

    // Antenna hole
    translate([width-ttgo_module_antenna_connector_length, hole_y, hole_z])
    rotate([0, 90, 0])
    linear_extrude(ttgo_module_antenna_connector_length) circle(d=ttgo_module_antenna_connector_diameter);
}

module versionstamp() {
    translate([width/2, 15, -.5])
    linear_extrude(version_depth)
    rotate([0, 180, 0])
    text(version_string, version_size, halign="center");
}

module components() {
    battery_holder();
    wire_guides();
    ttgo_module();
}

module box() {
    difference() {
        cube([width, length, height]);
        translate([inner_diff, inner_diff, inner_diff])
            cube([width-inner_diff*2, length-inner_diff*2, height-inner_diff]);
        translate([0, 0, battery_holder_height+padding_size+height_padding]) lid_base();
        antenna_connector();
        versionstamp();
    }
    difference() {
        translate([inner_diff, inner_diff, inner_diff])
            difference() {
                cube([width-inner_diff, length-inner_diff, 5]);
                components();
            }
        antenna_connector();
    }
}

box();
//components();
translate([width*2+10, 0, lid_height]) rotate([0, 180, 0]) lid();